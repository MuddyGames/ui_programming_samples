## Example of committing a Practical

Start by creating a repository and clone that repository to your computer as follows

1. Open a command prompt, type **Command** in windows
2. You will probaly be in the following present working directory **c:\users\username>**
3. If you have not done so already create a projects directory on your **C drive**
4. Issues command **cd ..** until your reach root of **C drive** **c:\/**
5. Make a directory **projects** using **mkdir projects** and **cd projects** into that directory
6. You should now be in the following directory **c:\projects>**
7. Now clone your repository **c:\projects>git clone #####\canvas.git** this is clone url for a "canvas" repository
8. Move to that directory cd **canvas** and using **cd canvas** you should now be in the directory **c:\projects\canvas>**
9. Create a sample file in the directory using **c:\projects\canvas>notepad practical.html** and save the file
10. Add file to local repository by issuing command **git add .**
11. Label that commit using **git commit -am "initial commit"**
12. Push new version to your online repository **git push**
13. Your new and updated files are now committed to the repository
14. Your files should now be uploaded
