// Each time this function is called a GameObject
// is create based on the arguments
// In JavaScript you can consider everything an Object
// including functions

function GameObject(name, image, health) {
    this.name = name;
    this.img = image; // this can be used to hold image filename
    this.health = health;
    this.x = 0; // initialised at 0 ***
    this.y = 0; // initialised at 0 ***
}

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
var gamerInput = new GamerInput("None"); //No Input

// Default Player
var player = new GameObject("Player", "test1.png", 100);

// Gameobjects is a collection of the Actors within the game
// this is an Array
var gameobjects = [player, new GameObject("NPC", "test2.png", 100)];

// Process keyboard input event
function input(event) {
    // Take Input from the Player
    // console.log("Input");
    // console.log("Event type: " + event.type);
    console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None"); //No Input
    }
    // console.log("Gamer Input :" + gamerInput.action);
}

// Run when attack clicked

function attack(){
    gamerInput = new GamerInput("Attack");
}

function update() {
    // Iterate through all GameObjects
    // Updating position and gamestate
    // console.log("Update");

    // Check is input is Attack

    if (gamerInput.action === "Attack") {
        console.log("Attacked");
    }

    for (i = 0; i < gameobjects.length; i++) {

        if (gamerInput.action === "Up") {
            gameobjects[i].health = 100;
            // console.log("Up");
        }

        if (gameobjects[i].health >= 1) {
            gameobjects[i].health = gameobjects[i].health - 1;
            // console.log("Health :" + gameobjects[i].health);

            // *** This is where X and Y are being updated
            if (gamerInput.action === "Down") {
                gameobjects[i].x += 1;
                gameobjects[i].y += 1;
                // console.log("Down");
            }

        } else {
            console.log(gameobjects[i].name + " at X: " + gameobjects[i].x + "  Y: " + gameobjects[i].y + " looks like its not alive :'(");
        }
    }
}

// Draw GameObjects to Console
// Modify to Draw to Screen
function draw() {
    // Clear Canvas
    // Iterate through all GameObjects
    // Draw each GameObject
    // console.log("Draw");
    for (i = 0; i < gameobjects.length; i++) {
        if (gameobjects[i].health > 0) {
            console.log("Image :" + gameobjects[i].img);
        }
    }
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

// Handle Keypressed "keyup" or "keydown"
// this is is being handled by the method input()
window.addEventListener('keyup', input);
window.addEventListener('keydown', input);

