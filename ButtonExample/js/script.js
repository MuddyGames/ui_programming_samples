// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
var gamerInput = new GamerInput("None"); //No Input

// Process keyboard input event
function input(event) {
    // Take Input from the Player
    // console.log("Input");
    // console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None"); //No Input
    }
    // console.log("Gamer Input :" + gamerInput.action);
}

// Run when Attack Button clicked
function attackButton(){
    gamerInput = new GamerInput("Attack");
}

// Run when Sheild Button clicked
function sheildButton(){
    gamerInput = new GamerInput("Sheild");
}

// Run when Spell Button clicked
function spellButton(){
    gamerInput = new GamerInput("Spell");
}

function update() {
    // Iterate through all GameObjects
    // Updating position and gamestate
    // console.log("Update");

    // Check Input

    if (gamerInput.action === "Attack") {
        console.log("Attack Enemy");
    }
    else if (gamerInput.action === "Sheild") {
        console.log("Hold Up Sheild");
    }
    else if (gamerInput.action === "Spell") {
        console.log("Casting Spell");
    }
    else if (gamerInput.action === "Up") {
        console.log("Move Up");
    }
    else if (gamerInput.action === "Down") {
        console.log("Move Down");
    }
    else if (gamerInput.action === "Left") {
        console.log("Move Left");
    }
    else if (gamerInput.action === "Right") {
        console.log("Move Right");
    }
}


// Draw GameObjects to Console
// Modify to Draw to Screen
function draw() {
    // Clear Canvas
    // Iterate through all GameObjects
    // Draw each GameObject
    // console.log("Draw");
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

// Handle Keypressed "keyup" or "keydown"
// this is is being handled by the method input()
window.addEventListener('keyup', input);
window.addEventListener('keydown', input);

