var canvas = document.getElementById('game')
var context = canvas.getContext('2d');

var score = 0;

function buttonOnClick() {
  // alert("Booooommmmmm!!!");
  console.log("Button Pressed");
  updateScore();
}

function onPageLoad() {
  // Using JSON and Local Storage for
  // GameState Management
  var gameObjects = {
    'pawn': 1,
    'worker': 2,
    'boss': 3
  };

  // Game objects as JSON
  localStorage.setItem('gameObjects', JSON.stringify(gameObjects));

  // Retrieve Games object as from storage
  var npcObjects = localStorage.getItem('gameObjects');

  console.log('npcObjects: ', JSON.parse(npcObjects));

  // Reading Level Information from a file
  var readJSONFromURL = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';

    xhr.onload = function () {
      var status = xhr.status;
      if (status == 200) {
        callback(null, xhr.response);
      } else {
        callback(status);
      }
    };

    xhr.send();
  };

  readJSONFromURL('./data/level.json', function (err, data) {
    if (err != null) {
      console.error(err);
    } else {
      var text = data["Pawns"];
      console.log(text);
      var text = data["Grunts"];
      console.log(text);
      var text = data["Boss"];
      console.log(text);
    }
  });

  // Reading File from a Server

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var data = JSON.parse(this.responseText);
      document.getElementById("NPC").innerHTML = data[0];
    }
  };
  xmlhttp.open("GET", "./data/level.json", true);
  xmlhttp.send();



  updateScore();

}

// Update the player score
function updateScore() {
  var current_score = localStorage.getItem('score');

  if (isNaN(current_score)) {
    localStorage.setItem('score', 0);
    document.getElementById("SCORE").innerHTML = " [ " + current_score + " ] ";
  } else {
    localStorage.setItem('score', parseInt(current_score) + 1);
    document.getElementById("SCORE").innerHTML = " [ " + current_score + " ] ";
  }

}

// Update Heads Up Display with Weapon Information
function weaponSelection() {
  var selection = document.getElementById("equipment").value;
  var active = document.getElementById("active");
  if (active.checked == true) {
    document.getElementById("HUD").innerHTML = selection + " active ";
    console.log("Weapon Active");
  } else {
    document.getElementById("HUD").innerHTML = selection + " selected ";
    console.log("Weapon Selected");
  }
}

// Draw a HealthBar on Canvas, can be used to indicate players health
function drawHealthbar() {
  var width = 100;
  var height = 20;
  var max = 100;
  var val = 10;

  // Draw the background
  context.fillStyle = "#000000";
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.fillRect(0, 0, width, height);

  // Draw the fill
  context.fillStyle = "#00FF00";
  var fillVal = Math.min(Math.max(val / max, 0), 1);
  context.fillRect(0, 0, fillVal * width, height);
}

// Array of Weapon Options
var options = [{
    "text": "Select a Weapon",
    "value": "No Weapon",
    "selected": true
  },
  {
    "text": "Spear",
    "value": "Javelin"
  },
  {
    "text": "Sword",
    "value": "Longsword"
  },
  {
    "text": "Crossbow",
    "value": "Pistol crossbow"
  }
];

var selectBox = document.getElementById('equipment');

for (var i = 0; i < options.length; i++) {
  var option = options[i];
  selectBox.options.add(new Option(option.text, option.value, option.selected));
}

drawHealthbar();