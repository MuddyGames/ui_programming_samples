// Get a handle to the canvas context
var canvas = document.getElementById("game");

// get 2D context for this canvas
var context = canvas.getContext("2d");

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject() {
    this.x = 0; // initialised at 0 ***
    this.y = 0; // initialised at 0 ***
}

// Image Atlas
function ImageAtlas() {
    this.x = 0; // initialised at 0 ***
    this.y = 0; // initialised at 0 ***
    this.w = 0; // initialised at 0 ***
    this.h = 0; // initialised at 0 ***
}

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
var gamerInput = new GamerInput("None"); //No Input

// Setup a new Image Atlas
var imageAtlas = new ImageAtlas()

// Setup Image to Draw
var image = new Image();
image.src = "./img/rock_paper_sissors.png";

// Default Player
var player = new GameObject();

// Functions to run on Page Load
function onPageLoad() {
    console.log("Page Loaded");
    processSelection(); // Setup default Selection
}

// Process keyboard input event
function input(event) {
    // Take Input from the Player
    // console.log("Input");
    // console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None"); //No Input
    }
    // console.log("Gamer Input :" + gamerInput.action);
}

// Run when Attack Button clicked
function attackButton() {
    gamerInput = new GamerInput("Attack");
}

// Run when Sheild Button clicked
function sheildButton() {
    gamerInput = new GamerInput("Sheild");
}

// Run when Spell Button clicked
function spellButton() {
    gamerInput = new GamerInput("Spell");
}

// Process selection
function processSelection() {
    var selection = document.getElementById("choice").value;

    if (selection === "Rock") {
        imageAtlas.x = 81;
        imageAtlas.y = 103;
        imageAtlas.w = 106;
        imageAtlas.h = 125;
    } else if (selection === "Paper") {
        imageAtlas.x = 208;
        imageAtlas.y = 65;
        imageAtlas.w = 104;
        imageAtlas.h = 151;
    } else if (selection === "Sissors") {
        imageAtlas.x = 325;
        imageAtlas.y = 61;
        imageAtlas.w = 91;
        imageAtlas.h = 153;
    } else {
        imageAtlas.x = 81;
        imageAtlas.y = 103;
        imageAtlas.w = 106;
        imageAtlas.h = 125;
    }

    document.getElementById("game").focus(); // Set the focus back on the canvas
}

function update() {
    // Iterate through all GameObjects
    // Updating position and gamestate
    // console.log("Update");

    // Check Input

    if (gamerInput.action === "Attack") {
        console.log("Attack Enemy");
    } else if (gamerInput.action === "Sheild") {
        console.log("Hold Up Sheild");
    } else if (gamerInput.action === "Spell") {
        console.log("Casting Spell");
    } else if (gamerInput.action === "Up") {
        console.log("Move Up");
        player.y -= 1; // Move Player Up
    } else if (gamerInput.action === "Down") {
        console.log("Move Down");
        player.y += 1; // Move Player Down
    } else if (gamerInput.action === "Left") {
        console.log("Move Left");
        player.x -= 1; // Move Player Left
    } else if (gamerInput.action === "Right") {
        console.log("Move Right");
        player.x += 1; // Move Player Right
    }
}


// Draw GameObjects to Console
// Modify to Draw to Screen
function draw() {
    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    // Iterate through all GameObjects
    // Draw each GameObject
    //console.log("Draw");
    // Drawing an image using the Atlas Coordinates (Rock, Paper Sissors are all part of one image file)
    context.drawImage(image, imageAtlas.x, imageAtlas.y, imageAtlas.w, imageAtlas.h, player.x, player.y, imageAtlas.w, imageAtlas.h);
    image, player.x, player.y
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

// Handle Keypressed "keyup" or "keydown"
// this is is being handled by the method input()
window.addEventListener('keyup', input);
window.addEventListener('keydown', input);