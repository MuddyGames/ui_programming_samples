function hello() {
    alert("Hello");
    console.log("Hello");
}

function pageLoaded() {

    // get a handle to the canvas context
    var canvas = document.getElementById("the_canvas");

    // get 2D context for this canvas
    var context = canvas.getContext("2d");

    // Image Object
    var image = new Image();

    // Need onload as local file is seen as dirty file
    image.onload = function () {
        // Draw image
        context.drawImage(image, 2300, 1300, 800, 800, 0, 0, 800, 800);
    }

    // Setting the image src
    image.src = "img/IMG_20180815_105624.jpg";

    console.log(image.src);

    // Message to the user
    alert("Images finished loading");

}